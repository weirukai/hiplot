%global _empty_manifest_terminate_build 0
Name:		python-hiplot
Version:	0.1.33
Release:	1
Summary:	High dimensional Interactive Plotting tool
License:	MIT Liscense
URL:		https://github.com/facebookresearch/hiplot
Source0:	https://files.pythonhosted.org/packages/81/f0/5b17e48ebfcbd9f8014ee76949091e640929714c29937ea3a04fde9ac488/hiplot-0.1.33.tar.gz
BuildArch:	noarch

Requires:	(python3-ipython(>=7.0.1))
Requires:	(python3-flask)
Requires:	(python3-flask-compress)
Requires:	(python3-beautifulsoup4)
Requires:	(python3-pytest)
Requires:	(python3-mypy)
Requires:	(python3-ipykernel)
Requires:	(python3-wheel)
Requires:	(python3-selenium)
Requires:	(python3-sphinx)
Requires:	(python3-mistune(==0.8.4))
Requires:	(python3-twine)
Requires:	(python3-guzzle-sphinx-theme)
Requires:	(python3-pre-commit)
Requires:	(python3-pandas)
Requires:	(python3-streamlit(>=0.63))
Requires:	(python3-beautifulsoup4)
Requires:	(python3-optuna)

%description
HiPlot is a lightweight interactive visualization tool to help AI researchers discover correlations and patterns in high-dimensional data using parallel plots and other graphical ways to represent information.





%package -n python3-hiplot
Summary:	High dimensional Interactive Plotting tool
Provides:	python-hiplot
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
%description -n python3-hiplot
HiPlot is a lightweight interactive visualization tool to help AI researchers discover correlations and patterns in high-dimensional data using parallel plots and other graphical ways to represent information.





%package help
Summary:	Development documents and examples for hiplot
Provides:	python3-hiplot-doc
%description help
HiPlot is a lightweight interactive visualization tool to help AI researchers discover correlations and patterns in high-dimensional data using parallel plots and other graphical ways to represent information.



%prep
%autosetup -n hiplot-0.1.33

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-hiplot -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sat Sep 09 2023 Python_Bot <Python_Bot@openeuler.org> - 0.1.33-1
- Package Spec generated
